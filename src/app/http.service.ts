import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from "rxjs";
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  baseUrl = 'http://localhost:4200/';
  hostUrl = 'http://localhost/my-project';
  hostUrl2 = 'http://127.0.0.1:8000';
  //   hostUrl = 'http://127.0.0.1:8000';

    headers = new Headers();
    options = new RequestOptions();


    constructor(private http: HttpClient, private http2: Http) {
        this.headers.append('Content-Type', 'application/x-ww-form-urlencoded');
        this.options.headers = this.headers;
    }
    get3 (endpoint){
        return this.http.get(this.hostUrl2 + endpoint)
    }

    post3 (endpoint, params){
        return this.http.post(this.hostUrl2 + endpoint, params)
    }

    get2 (endpoint){
        return this.http2.get(this.hostUrl + endpoint, this.options)
    }

    post2 (endpoint, params){
        return this.http2.post(this.hostUrl + endpoint, params, this.options)
    }

  get (endpoint){
    return this.http.get(this.baseUrl + endpoint);
  }

  post (endpoint, params){
    return this.http.post(this.baseUrl + endpoint, params);
  }
}
