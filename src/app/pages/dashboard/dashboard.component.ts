import {Component, OnDestroy} from '@angular/core';
import { NbThemeService } from '@nebular/theme';
import { takeWhile } from 'rxjs/operators/takeWhile' ;
import { NgForm } from '@angular/forms';

import { HttpService } from "../../http.service";

interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
}

@Component({
  selector: 'ngx-dashboard',
  styleUrls: ['./dashboard.component.scss'],
  templateUrl: './dashboard.component.html',
})
export class DashboardComponent implements OnDestroy {
  cards: any;


  starRate = 0;
  starRate2 = 0;
  starRate3 = 0;
  starRate4 = 0;
  starRate5 = 0;
  private alive = true;
  memo = {};
  memos = [];
  input = {};
  inputs = [];
  test = [];
  item = JSON.parse(localStorage.getItem('inputs'));
  isSubmitted: Boolean = false;
  firstSubmitted: Boolean = false;

  /*onSubmit(memoForm: NgForm) {
    this.isSubmitted = true;
    if (memoForm.valid) {
      this.memos.push({
        title : memoForm.controls.title.value,
        detail : memoForm.controls. detail.value
      });
      localStorage.setItem('memos', JSON.stringify(this.memos));
      this.isSubmitted = false;
      memoForm.resetForm();
    }
  }*/
    onSubmit(memoForm: NgForm) {
        this.isSubmitted = true;
        if (memoForm.valid) {
            this.http.post2('/saveMemo.php', this.memo).subscribe((data) => {
                if (data.status == 200) {
                    memoForm.resetForm();
                    this.isSubmitted = false;
                    this.getMemos();
                } else {
                    alert("ไม่สามารถบันทึกได้เนื่องจาก " + data);
                }
            });
        }
    }

    getMemos() {
        this.http.get2('/getMemos.php').subscribe((data) => {
            console.log(data)
            if (data.status == 200) {
                let jsonObject = data._body;
                // alert(jsonObject);
                console.log(jsonObject);
                localStorage.setItem('jsonObject', jsonObject)
                let color: string = jsonObject.substring (3,8);
                this.test = jsonObject;
                // alert(color);
                // this.test = JSON.parse(localStorage.getItem('jsonObject'));
            } else {
                alert("ไม่สามารถเชื่อมต่อข้อมูลได้เนื่องจาก" + data);
            }
        });
    }

  firstSubmit(firstForm: NgForm) {
    this.firstSubmitted = true;
    if (firstForm.valid) {
      this.inputs.push({
        inputFirstName : firstForm.controls.inputFirstName.value,
        inputLastName : firstForm.controls.inputLastName.value,
        inputNickName : firstForm.controls.inputNickName.value,
        inputPhoneNumber : firstForm.controls.inputPhoneNumber.value,
        inputAddress : firstForm.controls.inputAddress.value,
        inputWeight : firstForm.controls.inputWeight.value,
        inputHeight : firstForm.controls.inputHeight.value,
        inputImg : firstForm.controls.inputImg.value
      });
      if (this.item != null){
          // this.inputs = this.item;
      }
      localStorage.setItem('inputs', JSON.stringify(this.inputs));
      this.firstSubmitted = false;
      firstForm.resetForm();
    }
  }

  removeMemo(index) {
    this.memos.splice(index, 1);
    localStorage.setItem('memos', JSON.stringify(this.memos))
  }

  lightCard: CardSettings = {
    title: 'Light',
    iconClass: 'nb-lightbulb',
    type: 'primary',
  };
  rollerShadesCard: CardSettings = {
    title: 'Roller Shades',
    iconClass: 'nb-roller-shades',
    type: 'success',
  };
  wirelessAudioCard: CardSettings = {
    title: 'Wireless Audio',
    iconClass: 'nb-audio',
    type: 'info',
  };
  coffeeMakerCard: CardSettings = {
    title: 'Coffee Maker',
    iconClass: 'nb-coffee-maker',
    type: 'warning',
  };

  statusCards: string;

  commonStatusCardsSet: CardSettings[] = [
    this.lightCard,
    this.rollerShadesCard,
    this.wirelessAudioCard,
    this.coffeeMakerCard,
  ];

  statusCardsByThemes: {
    default: CardSettings[];
    cosmic: CardSettings[];
    corporate: CardSettings[];
  } = {
    default: this.commonStatusCardsSet,
    cosmic: this.commonStatusCardsSet,
    corporate: [
      {
        ...this.lightCard,
        type: 'warning',
      },
      {
        ...this.rollerShadesCard,
        type: 'primary',
      },
      {
        ...this.wirelessAudioCard,
        type: 'danger',
      },
      {
        ...this.coffeeMakerCard,
        type: 'secondary',
      },
    ],
  };

  constructor(private themeService: NbThemeService, private http: HttpService) {
    this.themeService.getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe(theme => {
        this.statusCards = this.statusCardsByThemes[theme.name];
    });
  }
  ngOnDestroy() {
    this.alive = false;
  }

  ngOnInit(){
      this.http.get('assets/card.json').subscribe((data) => {
          this.cards = data;
          localStorage.setItem('cards', JSON.stringify(data));
      });
      this.getMemos();
      this.testMemos();
  }
    testMemos() {
        this.http.get3('/getMemo').subscribe((data) => {
            // alert(data);
        });
    }
}
